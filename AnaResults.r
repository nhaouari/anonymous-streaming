#Library 
library("dplyr")
library("tidyr")

# Import results
results <- read.csv("~/results100.csv")

# Remove null values
df=results %>% filter(complete.cases(.)) 

# Agregate values based on the browser id which is different in each configuration by making a sum to the values 
agg_clientid=aggregate(select_if(df, is.numeric), by=list(NB_HS2=df$NB_HS,NB_Clients2=df$NB_Clients,client_uuid=df$client_uuid), FUN=sum)


# Agregate based on the number of clients and the number of hidden services 


agg_clientid$NB_Clients <- agg_clientid$NB_Clients2
agg_clientid$NB_HS <- agg_clientid$NB_HS2
agg_clientid$NB_Clients2  <- NULL 
agg_clientid$NB_HS2  <- NULL 
agg_clientid$client_uuid <- NULL


agg_nb_hs_clients=agg_clientid %>% group_by(NB_HS,NB_Clients) %>% summarise_all(funs(mean,sd))

agg_nb_hs_clients$x <- do.call(paste, c(agg_nb_hs_clients[c("NB_Clients", "NB_HS")], sep = " - "))


#Stock Bar 

qualities =agg_nb_hs_clients %>%
  select(x,q1_mean, q2_mean, q3_mean,q4_mean,MB_server_mean)


bars <- qualities %>% gather(Quality, Value, c(q1_mean, q2_mean, q3_mean,q4_mean))


bars$Percentage =round(bars$Value*100/bars$MB_server_mean,0)

ggplot(data=bars, aes(x=x, y=Percentage, fill=Quality )) +geom_bar(stat="identity", color="black")+labs(title="Quality distribution throught the different configuration",
        x ="Nb of clients - Nb of hidden services", y = "Percentage (%)")+
  theme_minimal()


#BoxPlots 
agg_clientid$x <- do.call(paste, c(agg_clientid[c("NB_Clients", "NB_HS")], sep = " - "))
agg_clientid$x <- ordered(agg_clientid$x, levels = c("2 - 8", "2 - 16","4 - 8", "4 - 16","16 - 8", "16 - 16","32 - 8", "32 - 16"))


boxplot(agg_clientid$q1 ~ agg_clientid$x,main = "Distribution of Q1 values in different configurations", 
        xlab = "Nb of clients - Nb of hidden services", ylab = "DataSize (kb)",ylim = c(0, 5000), varwidth = TRUE, 
        boxwex = 0.5, staplewex = 0.7, outpch = NA, col = terrain.colors(8))


boxplot(agg_clientid$q2 ~ agg_clientid$x,main = "Distribution of Q2 values in different configurations", 
        xlab = "Nb of clients - Nb of hidden services", ylab = "DataSize (kb)",ylim = c(0, 10000), varwidth = TRUE, 
        boxwex = 0.5, staplewex = 0.7, outpch = NA, col = terrain.colors(8))


boxplot(agg_clientid$q3 ~ agg_clientid$x,main = "Distribution of Q3 values in different configurations", 
        xlab = "Nb of clients - Nb of hidden services", ylab = "DataSize (kb)", ylim = c(0, 15000),varwidth = TRUE, 
        boxwex = 0.5, staplewex = 0.7, outpch = NA, col = terrain.colors(8))


boxplot(agg_clientid$q4 ~ agg_clientid$x,main = "Distribution of Q4 values in different configurations", 
        xlab = "Nb of clients - Nb of hidden services", ylab = "DataSize (kb)", ylim = c(0, 50000),varwidth = TRUE, 
        boxwex = 0.5, staplewex = 0.7, outpch = NA, col = terrain.colors(8))

boxplot(agg_clientid$sec_rebuf ~ agg_clientid$x,main = "Distribution of rebuffering time values in different configurations", 
        xlab = "Nb of clients - Nb of hidden services", ylab = "Time (seconds)", ylim = c(0, 40),varwidth = TRUE, 
        boxwex = 0.5, staplewex = 0.7, outpch = NA, col = terrain.colors(8))

boxplot(agg_clientid$glitches ~ agg_clientid$x,main = "Distribution of glitches in different configurations", 
        xlab = "Nb of clients - Nb of hidden services", ylab = "Number ", ylim = c(0, 6),varwidth = TRUE, 
        boxwex = 0.5, staplewex = 0.7, outpch = NA, col = terrain.colors(8))








